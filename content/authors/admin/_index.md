---
# Display name
title: Muhammad Abdullah Iqbal

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Research Assistant

# Organizations/Affiliations
organizations:
- name: National University of Sciences and Technology, Pakistan
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include material sciences, material physics and solid state physics.

interests:
- Batteries
- Supercapacitors
- Fuel Cells

education:
  courses:
  - course: PhD in Material Science and Engineering
    institution: Jiaotong University, China
    year: Enrolled
  - course: MS in Physics
    institution: National University of Sciences and Technology, Pakistan
    year: 2017
  - course: BS in Physics
    institution: University of Gujrat, Pakistan
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/TransformersBM
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: researchgate
  icon_pack: fab
  link: https://www.researchgate.net/profile/M_Abdullah_Iqbal
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/abdullah-iqbal-nust/
- icon: file-pdf
  icon_pack: far
  link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

Muhammad Abdullah Iqbal is a research assistant at PCSL at National University of Sciences and Technology (NUST). 
His research focuses on the chemical synthesis techniques of nanomaterials and nanohybrids. Additionally, he focuses on investigating the synthesized materials through different characterization techniques to find its optical, structural and electrical properties. Abdullah received his MS in Physics from the National University of Sciences and Technology (NUST) and BS in Physics from University of Gujrat. Abdullah has synthesized nanohybrids that can be used to degrade the organic dyes and colourless pollutants from water in short time under visible light. He received grant from Higher Education Commission of Pakistan to pursue his Master studies at NUST. During his Bachelor studies he was awarded with Most Dynamic Student of the Year for his dynamic nature for pursuing studies with volunteer and community works. During his job as Lecturer Physics he was awarded with Best Performer Award for his mentored students outperformed 170 colleges in a competition on environmental projects exhibition in Islamabad.
